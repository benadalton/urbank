$('#loginBtn').on('click', function(e){
  e.preventDefault();
  $('body').addClass('overlay');
  $('.modal').fadeIn();
});

$('#closeModal, .submit').on('click', function(e){
  e.preventDefault();
  $('body').removeClass('overlay');
  $('.modal').fadeOut();
});