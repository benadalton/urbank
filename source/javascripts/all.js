//= require jquery
//= require _tabs
//= require _modal
//= require _rates


$('.mobileNavIcon').on('click', function(){
  $('.navigation').slideToggle();
});

var resizeTimer;

$(window).on('resize', function(e) {

  clearTimeout(resizeTimer);
  resizeTimer = setTimeout(function() {

    // Run code here, resizing has "stopped"
    if($(window).width() > 768){
      $('.header .navigation').show();
    } else {
      $('.header .navigation').hide();
    }
  }, 250);

});