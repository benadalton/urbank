$('.tab-nav').on('click', 'a', function(e){
  e.preventDefault();
  var tab = $(this).parent();
  var content = $($(this).attr('href'));
  $('.tab-nav li').removeClass('active');
  $('.tab-content').removeClass('active');
  content.addClass('active');
  tab.addClass('active');
});