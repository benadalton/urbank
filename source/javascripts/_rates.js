$.ajax({
    url:"javascripts/code-test.json",
    dataType: 'json',
    success: function(bankData){
      var banks = [];
      bankData.forEach( function (bank){
        if(bank.name == "URBank") {
          banks.unshift( "<tr><td>" + bank.name + "</td><td class='number'>" + bank.apy + "</td><td class='number'>$" + bank.earnings.toFixed(2) + "</td></tr>" );
        }else{
          banks.push( "<tr><td>" + bank.name + "</td><td class='number'>" + bank.apy + "</td><td class='number'>$" + bank.earnings.toFixed(2) + "</td></tr>" );
        }
      });
      $(banks.join("")).appendTo( "#bankRates tbody" );
    },
    error: function(){
      console.log('Error retrieving json. Chrome will not allow access to local json files. Try Firefox or even IE9+.');
    }
});